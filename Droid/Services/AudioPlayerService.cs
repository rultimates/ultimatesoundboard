﻿using System;
using Android.Media;
using UltimateUberVetteSoundboard.Droid.Services;
using UltimateUberVetteSoundboard.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioPlayerService))]
namespace UltimateUberVetteSoundboard.Droid.Services
{
    public class AudioPlayerService : IAudioPlayerService
    {
        private MediaPlayer mediaPlayer;

        public void Play(string pathToAudioFile)
        {
            if (mediaPlayer != null)
            {
                mediaPlayer.Completion -= MediaPlayer_Completion;
                mediaPlayer.Stop();
            }

            var fullPath = pathToAudioFile;

            Android.Content.Res.AssetFileDescriptor afd = null;

            try
            {
                afd = Forms.Context.Assets.OpenFd(fullPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error openfd: " + ex);
            }
            if (afd != null)
            {
                System.Diagnostics.Debug.WriteLine("Length " + afd.Length);
                if (mediaPlayer == null)
                {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.Prepared += (sender, args) =>
                    {
                        mediaPlayer.Start();
                        mediaPlayer.Completion += MediaPlayer_Completion;
                    };
                }

                mediaPlayer.Reset();
                mediaPlayer.SetVolume(1.0f, 1.0f);

                mediaPlayer.SetDataSource(afd.FileDescriptor, afd.StartOffset, afd.Length);
                mediaPlayer.PrepareAsync();
            }
        }

        void MediaPlayer_Completion(object sender, EventArgs e)
        {
            OnFinishedPlaying?.Invoke();
        }

        public void Pause()
        {
            mediaPlayer?.Pause();
        }

        public void Play()
        {
            mediaPlayer?.Start();
        }

        public Action OnFinishedPlaying { get; set; }
    }
}