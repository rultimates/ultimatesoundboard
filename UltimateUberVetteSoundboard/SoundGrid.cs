﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace UltimateUberVetteSoundboard
{
    public class SoundGrid
    {
        public Grid Grid { get; set; }

        public SoundGrid(int noOfCols, Dictionary<string, string> sounds)
        {
            Grid = new Grid { BackgroundColor = SoundButton.BackGroundColor };

            for (int i = 0; i < noOfCols; i++)
            {
                Grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            }

            AddRows(noOfCols, sounds);

            AddSounds(noOfCols);
        }

        private void AddSounds(int noOfCols)
        {
            var x = 0;
            var y = 0;

            foreach (var snd in Sounds.Get.Keys)
            {
                var btn = new SoundButton(snd, GetColor(y), new PlayController(Sounds.Get[snd]));
                Grid.Children.Add(btn, x, y);
                x++;
                if (x % noOfCols == 0) { y++; x = 0; }
            }
        }

        private Color GetColor(int row)
        {
            switch (row + 6 % 6)
            {
                case 0:
                    {
                        return SoundButton.GreenColor;
                    }
                case 1:
                    {
                        return SoundButton.RedColor;
                    }
                case 2:
                    {
                        return SoundButton.BlueColor;
                    }
                case 3:
                    {
                        return SoundButton.LightBlueColor;
                    }
                case 4:
                    {
                        return SoundButton.PurpleColor;
                    }
                case 5:
                    {
                        return SoundButton.OrangeColor;
                    }
                default:
                    {
                        return Color.HotPink;
                    }
            }
        }

        private void AddRows(int noOfCols, Dictionary<string, string> sounds)
        {
            var row = 0;
            var col = 0;
            foreach (var unused in sounds.Keys)
            {
                if (col % noOfCols == 0)
                {
                    row++;
                    Grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(100, GridUnitType.Absolute) });
                }
                col++;
            }
        }
    }
}
