﻿using System;
using Xamarin.Forms;

namespace UltimateUberVetteSoundboard
{
    public class SoundButton : Button
    {
        public static Color BackGroundColor => Color.FromRgb(100, 100, 100);
        public static Color GreenColor => Color.FromRgb(155, 193, 118);
        public static Color RedColor => Color.FromRgb(241, 127, 152);
        public static Color BlueColor => Color.FromRgb(121, 194, 249);
        public static Color LightBlueColor => Color.FromRgb(159, 220, 212);
        public static Color PurpleColor => Color.FromRgb(178, 164, 251);
        public static Color OrangeColor => Color.FromRgb(235, 189, 127);

        private readonly PlayController _playController;

        public SoundButton(string caption, Color color, PlayController playController)
        {
            _playController = playController;
            Text = caption;
            BackgroundColor = BackGroundColor;
            BorderColor = color;
            TextColor = color;
            BorderWidth = 1;
            Clicked += SoundButton_Clicked;
            _playController.onFinished += () => IsEnabled = true;
        }

        void SoundButton_Clicked(object sender, EventArgs e)
        {
            _playController.PlayFile();
            IsEnabled = false;
        }
    }
}
