﻿using System;
using System.Threading.Tasks;
using UltimateUberVetteSoundboard.Services;
using Xamarin.Forms;

namespace UltimateUberVetteSoundboard
{
    public class PlayController
    {
        private IAudioPlayerService audioService;
        public Action onFinished { get; set; }
        private string File { get; }

        public PlayController(string file)
        {
            File = file;
            audioService = DependencyService.Get<IAudioPlayerService>();
            audioService.OnFinishedPlaying += AudioService_OnFinishedPlaying;
        }

        public async void PlayFile()
        {
            await Task.Run(() => audioService.Play(File));
        }

        void AudioService_OnFinishedPlaying()
        {
            onFinished?.Invoke();
        }
    }
}