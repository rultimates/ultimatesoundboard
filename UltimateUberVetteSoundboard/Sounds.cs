﻿using System;
using System.Collections.Generic;

namespace UltimateUberVetteSoundboard
{
    public static class Sounds
    {
        private static Dictionary<string, string> sounds;

        static Sounds()
        {
			sounds = new Dictionary<string, string>();
			sounds.Add("ba dum tss", "ba_dum_tss.mp3");
			sounds.Add("disco dance", "disco_dance.mp3");
			sounds.Add("DJ-Lazer", "DJ-Lazer.mp3");
			sounds.Add("DJ-Lazer-2", "DJ-Lazer-2.mp3");
			sounds.Add("DJ-Scratch", "DJ-Scratch.mp3");
			sounds.Add("DJ-Scratch-2", "DJ-Scratch-2.mp3");
			sounds.Add("erik doe rustig", "erik_doe_rustig.mp3");
			sounds.Add("erik heftige vibes", "erik_heftige_vibes.mp3");
			sounds.Add("erik niet aan je werken", "erik_niet_aan_je_werken.mp3");
			sounds.Add("erik van de lijn", "erik_van_de_lijn.mp3");
			sounds.Add("erik zo gefokt", "erik_zo_gefokt.mp3");
			sounds.Add("Ey enneeeh 01", "Ey_enneeeh_01.mp3");
			sounds.Add("Ey enneeeh 02", "Ey_enneeeh_02.mp3");
			sounds.Add("Ey enneeeh 03", "Ey_enneeeh_03.mp3");
			sounds.Add("fail", "fail.mp3");
			sounds.Add("Fred Wilma", "Fred Wilma.mp3");
			sounds.Add("guido", "guido.mp3");
			sounds.Add("horn", "horn.mp3");
			sounds.Add("maak dat ge weg zijt", "maak_dat_ge_weg_zijt.mp3");
			sounds.Add("mijn publiek", "mijnpubliek.mp3");
			sounds.Add("nein nein nein", "nein_nein_nein.mp3");
			sounds.Add("No-munnie", "No-munnie.mp3");
			sounds.Add("raadt t nooit", "raadt_t_nooit.mp3");
			sounds.Add("senks for watching", "senks for watching.mp3");
			sounds.Add("station", "station.mp3");
			sounds.Add("wiekser", "wiekser.mp3");
			sounds.Add("prutsers", "Prutsers.mp3");
			sounds.Add("puinbak", "PuinbakMartijn.mp3");
        }

        public static Dictionary<string, string> Get => sounds;
    }
}
