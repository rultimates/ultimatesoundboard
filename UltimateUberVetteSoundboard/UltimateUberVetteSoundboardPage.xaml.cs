﻿using Xamarin.Forms;

namespace UltimateUberVetteSoundboard
{
    public partial class UltimateUberVetteSoundboardPage : ContentPage
    {

        public UltimateUberVetteSoundboardPage()
        {
            InitializeComponent();

			this.Content = new ScrollView { Content = new SoundGrid(4, Sounds.Get).Grid };
        }
    }
}
