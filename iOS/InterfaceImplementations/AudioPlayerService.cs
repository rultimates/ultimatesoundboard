﻿using System;
using System.Collections.Generic;
using AVFoundation;
using Foundation;
using UltimateUberVetteSoundboard.iOS.InterfaceImplementations;
using UltimateUberVetteSoundboard.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioPlayerService))]
namespace UltimateUberVetteSoundboard.iOS.InterfaceImplementations
{
    public class AudioPlayerService : IAudioPlayerService
    {
        private Dictionary<string, AVAudioPlayer> _audioPlayers = new Dictionary<string, AVAudioPlayer>();

        public Action OnFinishedPlaying { get; set; }

        public void Play(string pathToAudioFile)
        {
            if (!_audioPlayers.ContainsKey(pathToAudioFile))
            {
                _audioPlayers.Add(pathToAudioFile, AVAudioPlayer.FromUrl(NSUrl.FromFilename(pathToAudioFile)));
            }
            AVAudioPlayer audioPlayer = _audioPlayers[pathToAudioFile];

            // Check if _audioPlayer is currently playing
            if (audioPlayer.Playing)
            {
                audioPlayer.FinishedPlaying -= Player_FinishedPlaying;
                audioPlayer.Stop();
            }

            audioPlayer.FinishedPlaying += Player_FinishedPlaying;
            audioPlayer.Play();
        }

        private void Player_FinishedPlaying(object sender, AVStatusEventArgs e)
        {
            OnFinishedPlaying?.Invoke();
        }

        public void Pause()
        {
           // _audioPlayer?.Pause();
        }

        public void Play()
        {
           // _audioPlayer?.Play();
        }
    }
}